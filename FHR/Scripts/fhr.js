﻿/// <reference path="jquery-2.1.1.js" />


var FHR = (function () {
    var latitude;
    var longitude;

    function setCurrentPosition() {
        window.navigator.geolocation.getCurrentPosition(
            function (position) {
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                getByName();
            },
            function (position) {
                getByName();
            })
    }

    function getByName() {
        var name = ($('#searchText').val());
        var data = {
            name: name,
            pageSize: 50,
            sortOptionKey: 'distance'
        }

        // Set geolocation if present
        if (latitude) {
            data.latitude = latitude;
        }
        if (longitude) {
            data.longitude = longitude;
        }

        $.ajax({
            url: 'http://api.ratings.food.gov.uk/Establishments',
            cache: false,
            data: data,
            dataType: 'json',
            headers: {
                'x-api-version': 2
            }

        }).done(processResults)
    }

    function processResults(data) {
        var rowNode, itemNode, itemNode2, nameNode, addressNode, addressText, addressKey, distance, image;

        // Clear results first
        $('#results').html('');

        // Loop through results and populate
        $.each(data.establishments, function (index, value) {

            // Create row
            rowNode = $('<div class="row list-group-item"></div>');
            itemNode = $('<div class="col-xs-8"></div>');
            itemNode2 = $('<div class="col-xs-4" style="text-align:right"></div>');

            // Create name
            distance = (value.Distance && value.Distance.toFixed(2) + ' miles') || '';
            nameNode = $('<h5 class="list-group-item-heading"></h5>').text(value.BusinessName);

            // Create address
            addressText = '';
            for (var i = 1; i < 5; i++) {
                addressKey = 'AddressLine' + i;
                if (value[addressKey]) {
                    if (addressText) {
                        addressText += ', ';
                    }
                    addressText += value[addressKey];
                }
            }
            addressNode = $('<p class="list-group-item-text small"></p>').html(addressText + '<br/><span class="distance">' + distance + '</span>');

            // Add data to column1
            nameNode.appendTo(itemNode);
            addressNode.appendTo(itemNode);

            // Get image
            image = $('<img></img>');
            image.attr('src', '/Content/' + value.RatingKey + '.jpg');
            image.appendTo(itemNode2);

            // Add columns to row
            itemNode.appendTo(rowNode);
            itemNode2.appendTo(rowNode);
            rowNode.appendTo($('#results'));
        })
        $('#searchButton').focus();
    }

    return {
        getByName: getByName,
        setCurrentPosition: setCurrentPosition
    }
}());

$(document).ready(function () {

    // Get current position
    FHR.setCurrentPosition();

    // Attach handlers
    $('#searchButton').on('click', FHR.getByName);
    $('#searchText').on('keypress', function (event) {
        if (event.which == 13) {
            FHR.getByName();
        }
    });
});
